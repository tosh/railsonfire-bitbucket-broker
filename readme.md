# a simple bitbucket broker for railsonfire.com

When invoked, it will parse and format bitbucket's payload so that railsonfire can make use of it.

Contents:

- broker.py - the RailsonfireBroker class
- brokers.py - a fake bitbucket brokers.BaseBroker class

# Thank you

* westhomas who provided the source and a test suite for the [deployhq broker]('https://bitbucket.org/westhomas/deployhq-bitbucket-broker/')