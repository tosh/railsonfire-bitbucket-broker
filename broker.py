import urllib
from brokers import BaseBroker
import json
 
PAYLOAD_RECEIVER = 'https://www.railsonfire.com/hook/%s'

class URLOpener(urllib.FancyURLopener):
    version = 'bitbucket.org'
 
class RailsonfireBroker(BaseBroker):
    def handle(self, payload):
        api_endpoint = PAYLOAD_RECEIVER
        railsonfire_project_id = payload['service']['project_id']
        url = api_endpoint % railsonfire_project_id

        ref = 'refs/head/none'

        if len(payload.get('commits', [])):
            if payload.get('commits')[0].get('branch', None):
                ref = 'refs/head/%s' % payload.get('commits')[0].get('branch')

        commits = []
        for commit in payload.get('commits', []):
            commit = {'id': commit.get('raw_node', None),
                      'message': commit.get('message', None),
                      'author': {'name': commit.get('raw_author', None), 'email': ''},
                      }
            commits.append(commit)

        railsonfire_payload = {'ref': ref,
                               'commits': commits
                              }

 
        railsonfire_payload = json.dumps(railsonfire_payload)
 
        opener = self.get_local('opener', URLOpener)
        import logging
        logging.error(url)
        logging.error(railsonfire_payload)
        opener.open(url, urllib.urlencode(dict(payload=railsonfire_payload)))